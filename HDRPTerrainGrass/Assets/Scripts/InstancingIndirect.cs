using UnityEngine;
using System.Collections;

/// <summary>
/// Used by TerrainDetailInstancing to instance objects with the GPU.
/// Does NOT utilize a compute shader for the position computations (use InstancingIndirectCS for that).
/// </summary>
public class InstancingIndirect : IInstancing
{
    private ComputeBuffer computeBuffer;

    private MaterialPropertyBlock materialProperty;

    private ITerrainDetailComputeKernel computeKernel;
    private IInstancing.VolatileData nextDataUpdate;
    private IInstancing.VolatileData dataUpdate;
    private IInstancing.FixedData data;
    private bool bufferIsDirty;

    static readonly int windDirectionId = Shader.PropertyToID("_WindDirection");
    static readonly int windMainId = Shader.PropertyToID("_WindMain");
    static readonly int windTurbulenceId = Shader.PropertyToID("_WindTurbulence");
    static readonly int windPulseFrequencyId = Shader.PropertyToID("_WindPulseFrequency");
    static readonly int windPulseMagnitudeId = Shader.PropertyToID("_WindPulseMagnitude");
    static readonly int volatileInstancingDataId = Shader.PropertyToID("_VolatileInstancingData");

    public InstancingIndirect(IInstancing.FixedData newData)
    {
        data = newData;
        dataUpdate = new IInstancing.VolatileData();
        dataUpdate.Clear();
        nextDataUpdate = new IInstancing.VolatileData();
        nextDataUpdate.Clear();
        bufferIsDirty = true;

        // this links the class back to CPU based positioning, i.e. no compute shader is used to compute positions.
        computeKernel = new TerrainDetailKernelInstancingDirect(this);
    }

    public override IEnumerator UpdateData(IInstancing.VolatileData newDataUpdate)
    {
        materialProperty = new MaterialPropertyBlock();
        materialProperty.SetColor("_BaseColor", data.color);

        this.nextDataUpdate = newDataUpdate;
        this.nextDataUpdate.FixData();
        this.bufferIsDirty = true;

        yield return null;
    }

    public override void DrawMeshes()
    {
        if(bufferIsDirty)
        {
            this.dataUpdate = this.nextDataUpdate;
            UpdateBuffer(dataUpdate.positions, dataUpdate.rotations, dataUpdate.scales);
        }
        if(computeBuffer == null)
        {
            return;
        }

        // set wind properties
        if (this.data.windZone != null)
        {
            data.material.SetVector(windDirectionId, this.data.windZone.transform.forward);
            data.material.SetFloat(windMainId, this.data.windZone.windMain * this.data.detailWindStrength);
            data.material.SetFloat(windTurbulenceId, this.data.windZone.windTurbulence);
            data.material.SetFloat(windPulseFrequencyId, this.data.windZone.windPulseFrequency);
            data.material.SetFloat(windPulseMagnitudeId, this.data.windZone.windPulseMagnitude);
        }
        else
        {
            data.material.SetFloat(windMainId, 0);
        }

        Graphics.DrawMeshInstancedProcedural(data.mesh, 0, data.material, this.dataUpdate.bounds, dataUpdate.positions.Length, materialProperty, data.castShadows);
    }

    private void UpdateBuffer(Vector3[] positions, Quaternion[] rotations, Vector3[] scales)
    {
        if (computeBuffer != null)
            computeBuffer.Release();
        bufferIsDirty = false;

        if (positions.Length == 0)
        {
            computeBuffer = null;
            return;
        }

        UnityEngine.Random.InitState(1);
        var bufferData = new ComputeBufferData[positions.Length];
        for (int i = 0; i < positions.Length; i++)
        {
            bufferData[i] = new ComputeBufferData();
            bufferData[i]._Matrix = new Matrix4x4();
            bufferData[i]._Matrix.SetTRS(positions[i], rotations[i], scales[i]);
            bufferData[i]._MatrixInverse = bufferData[i]._Matrix.inverse;
            bufferData[i]._Info = new Vector4(UnityEngine.Random.value, 0, 0, 0); // can be used to pass arbitrary, instance based data to the shader.
        }

        computeBuffer = new ComputeBuffer(positions.Length, CB_SIZE);
        computeBuffer.SetData(bufferData);

        data.material.SetBuffer(volatileInstancingDataId, computeBuffer);
    }

    public override IEnumerator RecomputeDetailData(int prototypeIndex, Terrain terrain, TerrainDetailInstancing parent)
    {
        yield return computeKernel.RecomputeDetailData(prototypeIndex, terrain, parent);
    }

    public override void Release()
    {
        OnDisable();
    }

    public void OnDisable()
    {
        if (computeBuffer != null)
            computeBuffer.Release();
        computeBuffer = null;
    }

    public override int Count()
    {
        return this.dataUpdate.positions == null ? 0 : this.dataUpdate.positions.Length;
    }
}
