using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainDetailKernelInstancingDirect : ITerrainDetailComputeKernel
{
    private IInstancing Instancer;

    public TerrainDetailKernelInstancingDirect(IInstancing instancer)
    {
        this.Instancer = instancer;
    }

    public IEnumerator RecomputeDetailData(int prototypeIndex, Terrain terrain, TerrainDetailInstancing parent, bool cullByDistance = true)
    {
        // prepare all the necessary information
        var detailPrototype = terrain.terrainData.detailPrototypes[prototypeIndex];
        var detailWidth = terrain.terrainData.detailWidth;
        var detailMap = terrain.terrainData.GetDetailLayer(0, 0, detailWidth, detailWidth, prototypeIndex);
        var noiseSpread = detailPrototype.noiseSpread;
        var cameraPos = Camera.main.transform.position;
        var detailDistance = terrain.detailObjectDistance;

        if (!Application.isPlaying && parent.UseInEditor)
        {
            cameraPos = UnityEditor.SceneView.lastActiveSceneView.camera.transform.position;
        }

        var localDetailData = new IInstancing.VolatileData();
        localDetailData.Clear();
        var detailScale = new Vector2(1f / detailWidth * terrain.terrainData.size.x, 1f / detailWidth * terrain.terrainData.size.z);

        var detailWidthDistance = Mathf.Max(Mathf.CeilToInt(detailDistance / detailScale.x),
                                            Mathf.CeilToInt(detailDistance / detailScale.y));
        Vector3 terrainCameraPos = terrain.transform.worldToLocalMatrix * cameraPos;
        // We have to flip x and z of the camera pos, since ix and iz iterate over an image.
        var detailCameraPos = new Vector2(terrainCameraPos.z / detailScale.y,
                                          terrainCameraPos.x / detailScale.x);
        var ixStart = Mathf.Max(0, Mathf.FloorToInt(detailCameraPos.x - detailWidthDistance));
        var izStart = Mathf.Max(0, Mathf.FloorToInt(detailCameraPos.y - detailWidthDistance));
        var ixEnd = Mathf.Min(detailWidth, ixStart + 2 * detailWidthDistance);
        var izEnd = Mathf.Min(detailWidth, izStart + 2 * detailWidthDistance);

        if(!cullByDistance)
        {
            ixStart = 0;
            izStart = 0;
            ixEnd = detailWidth;
            izEnd = detailWidth;
        }

        var detailSpaceMaxSqr = Mathf.Max(detailDistance / detailScale.y, detailDistance / detailScale.x);
        detailSpaceMaxSqr *= detailSpaceMaxSqr;

        var instanceCounter = 0; // used for coroutine management

        // now, process all the detail cells in range of the Camera.main
        for (int ix = ixStart; ix < ixEnd; ix++)
        {
            for (int iz = izStart; iz < izEnd; iz++)
            {
                // If the current cell is not in the DetailWidth radius, skip it.
                var detailSpaceDistanceX = (ix - detailCameraPos.x) * (ix - detailCameraPos.x);
                var detailSpaceDistanceZ = (iz - detailCameraPos.y) * (iz - detailCameraPos.y);
                var detailSpaceDistanceSqr = detailSpaceDistanceX + detailSpaceDistanceZ;
                if (detailSpaceDistanceSqr > detailSpaceMaxSqr && cullByDistance)
                {
                    continue;
                }

                // fix the random seed based on the current cell, so that the detail meshes for single cells are
                // always constructed the same way.
                Random.InitState(ix * 7919 + iz * 5309 + prototypeIndex * 3359);
                if (detailMap[ix, iz] > 0 && UnityEngine.Random.value < parent.DetailDensity)
                {
                    var pos = new Vector3();
                    pos.x = detailScale.x * iz + (detailScale.x * 0.5f);
                    pos.z = detailScale.y * ix + (detailScale.y * 0.5f);

                    pos.x -= (detailScale.x * (UnityEngine.Random.value - .5f) * noiseSpread);
                    pos.z += (detailScale.y * (UnityEngine.Random.value - .5f) * noiseSpread);

                    // Note that terrain.transform.LocalToWorld(pos) should not be used, because the terrain scale/rotation are not adjusted
                    pos += terrain.transform.position;
                    pos.y = terrain.SampleHeight(pos) + terrain.transform.position.y;

                    var cameraVector = cameraPos - pos;
                    var distance = cameraVector.magnitude;
                    if (distance < detailDistance || !cullByDistance)
                    {
                        var scaleHeight = detailPrototype.minHeight + UnityEngine.Random.value * (detailPrototype.maxHeight - detailPrototype.minHeight);
                        var scaleWidth = detailPrototype.minWidth + UnityEngine.Random.value * (detailPrototype.maxWidth - detailPrototype.minWidth);

                        var fadeDistance = Mathf.Clamp01((detailDistance - distance) / parent.FadeRange);
                        var scaleFade = (cullByDistance && parent.FadeDetailsAtDistance) ? Mathf.Lerp(0, 1, fadeDistance) : 1;

                        localDetailData.Add(
                            pos,
                            scaleFade * new Vector3(scaleWidth, scaleHeight, scaleWidth),
                            Quaternion.Euler(0, UnityEngine.Random.value * 360, 0));
                    }

                    instanceCounter++;
                    if (instanceCounter % parent.InstancesPerFrame == 0 && (Application.isPlaying || !parent.UseInEditor))
                    {
                        yield return new WaitForEndOfFrame();
                    }
                }
            }
        }

        yield return Instancer.UpdateData(localDetailData);
    }
}
