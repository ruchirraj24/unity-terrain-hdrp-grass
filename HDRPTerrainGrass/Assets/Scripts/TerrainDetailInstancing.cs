using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof(Terrain))]
[ExecuteAlways]
public class TerrainDetailInstancing : MonoBehaviour
{
    [Tooltip("Controls the detail density on the terrain.")]
    [Range(0, 1f)]
    public float DetailDensity = 1f;

    [Tooltip("Interval between updates of the Instancing object positions (in seconds).")]
    [Range(0, 10f)]
    public float UpdateInterval = 5f;

    [Tooltip("Enables distance based fading of objects.")]
    public bool FadeDetailsAtDistance = true;
    [Range(0.1f, 100f)]
    [Tooltip("Controls how far from the terrain Detail Distance the fading effect is visible.")]
    public float FadeRange = 4;

    [Min(1)]
    [Tooltip("Number of instances that get updated per frame.")]
    public int InstancesPerFrame = 50000;

    [Tooltip("Wind effect. The dry color of Details in the terrain controls how much individual meshes are affected.")]
    public WindZone WindZone;

    [Tooltip("Used for all billboards.")]
    public Material BillboardMaterial;

    [Tooltip("Sets shadow mode of all details.")]
    public ShadowCastingMode castShadows = ShadowCastingMode.On;

    [Tooltip("Deactivate for higher performance in the editor. In this case, it will only work in Play Mode.")]
    public bool UseInEditor = true;

    [Tooltip("Compute shader to initialize the detail matrices. Necessary for the GPU positioning computations.")]
    public ComputeShader IndirectInstancingComputeShader;

    [Tooltip("Detail meshes may be instanced and positioned only on the GPU, or also use the CPU. GPU positioning needs hardware with compute shader capabilities.")]
    public InstancingType UseProceduralInstancing = InstancingType.POS_CPU_INSTANCE_CPU;

    public enum InstancingType
    {
        POS_CPU_INSTANCE_CPU,
        POS_CPU_INSTANCE_GPU,
        POS_GPU_INSTANCE_GPU
    }

    [HideInInspector]
    [SerializeField]
    public Terrain terrain;
    private IInstancing[] instancers;
    private float lastUpdateTime = float.MinValue;

    public void Start()
    {
        if (Application.isPlaying || UseInEditor)
        {
            RecreateInstancers();
        }
    }

    public void RecreateInstancers()
    {
        terrain = GetComponent<Terrain>();
        // this is to remove the detail object meshes that unity draws itself
        terrain.detailObjectDensity = 0;

        var details = terrain.terrainData.detailPrototypes;

        ClearInstancers();

        instancers = new IInstancing[details.Length];
        for (int i = 0; i < details.Length; i++)
        {
            MeshFilter mf;
            MeshRenderer mr;
            var currentInstancerData = new IInstancing.FixedData
            {
                color = details[i].healthyColor,
                castShadows = castShadows,
                isBillboard = details[i].renderMode == DetailRenderMode.GrassBillboard,
                windZone = WindZone,
                detailWindStrength = details[i].dryColor.grayscale
            };

            if (details[i].usePrototypeMesh)
            {
                mf = details[i].prototype.GetComponent<MeshFilter>();
                mr = details[i].prototype.GetComponent<MeshRenderer>();

                if (mf == null)
                {
                    // try first lod
                    if (details[i].prototype.GetComponent<LODGroup>() != null && details[i].prototype.GetComponent<LODGroup>().GetLODs().Length > 0)
                    {
                        if (details[i].prototype.GetComponent<LODGroup>().GetLODs()[0].renderers.Length > 0)
                        {
                            mf = details[i].prototype.GetComponent<LODGroup>().GetLODs()[0].renderers[0].GetComponent<MeshFilter>();
                            mr = details[i].prototype.GetComponent<LODGroup>().GetLODs()[0].renderers[0].GetComponent<MeshRenderer>();
                        }
                    }
                    // TODO: use other LOD layers?
                }

                if (mf != null)
                {
                    currentInstancerData.mesh = mf.sharedMesh;
                }
                if(mr != null)
                {
                    // copy the material so that we can independently change its properties (mainly for instancing)
                    currentInstancerData.material = new Material(mr.sharedMaterial);
                }
            }
            else
            {
                currentInstancerData = CreateBillboard(details[i].prototypeTexture, currentInstancerData);
            }

            if (currentInstancerData.mesh != null)
            {
                var editorInstancing = UseProceduralInstancing;
                if(!Application.isPlaying)
                {
                    // do not use GPU/GPU in editor mode, because a lot is being sent around on the GPU in this mode.
                    editorInstancing = (editorInstancing == InstancingType.POS_GPU_INSTANCE_GPU) ? InstancingType.POS_CPU_INSTANCE_GPU : editorInstancing;
                }

                switch (editorInstancing)
                {
                    case InstancingType.POS_CPU_INSTANCE_CPU:
                        // this is fully CPU based.
                        instancers[i] = new InstancingDirect(currentInstancerData);
                        break;
                    case InstancingType.POS_CPU_INSTANCE_GPU:
                        // this can be used if drawing should be GPU based, but positionining is still CPU based.
                        instancers[i] = new InstancingIndirect(currentInstancerData);
                        break;
                    case InstancingType.POS_GPU_INSTANCE_GPU:
                        // this is fully GPU based, instancing and positioning.
                        instancers[i] = new InstancingIndirectCS(currentInstancerData, IndirectInstancingComputeShader);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                Debug.LogError("Detail object #" + i + " is missing MeshRenderer or MeshFilter.");
            }
        }
    }

    /// <summary>
    /// Create a plane mesh that acts as a billboard,
    /// and assign mesh and billboard material to the given InstanceData object.
    /// </summary>
    /// <param name="billboardTexture"></param>
    /// <param name="currentInstancer"></param>
    /// <returns>the currentInstancer object</returns>
    private IInstancing.FixedData CreateBillboard(Texture billboardTexture, IInstancing.FixedData currentInstancer)
    {
        var relativeWidth = billboardTexture.width / (float)billboardTexture.height;
        currentInstancer.mesh = CreateBillboardMesh(relativeWidth, 1f);

        var billboardMaterial = new Material(BillboardMaterial)
        {
            mainTexture = billboardTexture
        };
        currentInstancer.material = billboardMaterial;
        return currentInstancer;
    }

    private Mesh CreateBillboardMesh(float width, float height)
    {
        var mesh = new Mesh();

        // create vertices
        var vertices = new Vector3[4];
        vertices[0] = new Vector3(-width/2f, 0, 0);
        vertices[1] = new Vector3(width/2f, 0, 0);
        vertices[2] = new Vector3(width/2f, height, 0);
        vertices[3] = new Vector3(-width/2f, height, 0);

        // create triangles
        var triangles = new int[6];
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 3;
        triangles[3] = 3;
        triangles[4] = 1;
        triangles[5] = 2;

        // create uvs
        var uvs = new Vector2[4];
        uvs[0] = new Vector2(0, 0);
        uvs[1] = new Vector2(1, 0);
        uvs[2] = new Vector2(1, 1);
        uvs[3] = new Vector2(0, 1);

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;

        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();

        return mesh;
    }

    public void Update()
    {
        if (!UseInEditor && !Application.isPlaying)
        {
            ClearInstancers();
            return;
        }

        // Always update the instancers so that meshes are drawn every frame.
        if (instancers != null && instancers.Length > 0 && (Application.isPlaying || UseInEditor))
        {
            for (int iLayer = 0; iLayer < instancers.Length; iLayer++)
            {
                instancers[iLayer].DrawMeshes();
            }
        }

        // Update the entire instance layer every UpdateInterval.
        // If we are using GPU/GPU instancing, we can update every frame.
        if (Time.time - lastUpdateTime < UpdateInterval && (!Application.isPlaying || UseProceduralInstancing != InstancingType.POS_GPU_INSTANCE_GPU))
        {
            return;
        }
        lastUpdateTime = Time.time;

        if (instancers != null)
        {
            for (int iLayer = 0; iLayer < instancers.Length; iLayer++)
            {
                if (instancers[iLayer] != null)
                {
                    StartCoroutine(instancers[iLayer].RecomputeDetailData(iLayer, terrain, this));
                }
            }
        }
    }

    private void ClearInstancers()
    {
        if (instancers != null)
        {
            for (int i = 0; i < instancers.Length; i++)
            {
                if (instancers[i] != null)
                {
                    instancers[i].Release();
                }
            }
            instancers = null;
        }
    }

    public void OnDisable()
    {
        ClearInstancers();
    }

    public int DetailCount()
    {
        if (instancers == null)
            return 0;
        var count = 0;

        foreach (var instancer in instancers)
        {
            count += instancer.Count();
        }

        return count;
    }
}
