using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Fully GPU based instancing and positioning.
/// </summary>
public class InstancingIndirectCS : IInstancing
{
    /// <summary>
    /// This buffer contains all positioning information the compute shader has available.
    /// </summary>
    private ComputeBuffer fullComputeBuffer;
    /// <summary>
    /// This is the buffer that is used to actually instance the meshes. It is filled by the compute shader.
    /// </summary>
    private ComputeBuffer volatileComputeBuffer;

    /// <summary>
    /// This buffer is used to contain information about the volatile compute buffer.
    /// </summary>
    private ComputeBuffer bufferWithVolatileArgs;
    private uint[] volatileArgs = new uint[5] { 0, 0, 0, 0, 0 };

    private MaterialPropertyBlock materialProperty;

    private FixedData data;

    private ComputeShader computeShader;

    private int lastVolatileComputeBufferCount;
    private int resolution = 8;
    private Bounds bounds;

    private bool fullBufferInitialized
    {
        get
        {
            return fullComputeBuffer != null;
        }
    }

    static readonly int windDirectionId = Shader.PropertyToID("_WindDirection");
    static readonly int windMainId = Shader.PropertyToID("_WindMain");
    static readonly int windTurbulenceId = Shader.PropertyToID("_WindTurbulence");
    static readonly int windPulseFrequencyId = Shader.PropertyToID("_WindPulseFrequency");
    static readonly int windPulseMagnitudeId = Shader.PropertyToID("_WindPulseMagnitude");

    static readonly int cameraPositionId = Shader.PropertyToID("_CameraPosition");
    static readonly int cameraDirectionId = Shader.PropertyToID("_CameraDirection");
    static readonly int cullDistanceId = Shader.PropertyToID("_CullDistance");
    static readonly int cullAngleId = Shader.PropertyToID("_CullAngle");
    static readonly int fadeDistanceId = Shader.PropertyToID("_FadeDistance");

    static readonly int resolutionId = Shader.PropertyToID("_Resolution");
    static readonly int totalObjectsId = Shader.PropertyToID("_TotalObjects");

    // the data is moved like this: Full -> Volatile -> Consume -> Fixed.
    static readonly int fullBufferId = Shader.PropertyToID("_FullInstancingData");
    static readonly int volatileBufferId = Shader.PropertyToID("_VolatileInstancingData");

    public InstancingIndirectCS(FixedData newData, ComputeShader shader)
    {
        lastVolatileComputeBufferCount = -1;
        computeShader = GameObject.Instantiate(shader);
        data = newData;
        bounds = new Bounds();
    }

    /// <summary>
    /// This is only called when the full buffer should be updated.
    /// </summary>
    /// <param name="newDataUpdate"></param>
    public override IEnumerator UpdateData(VolatileData newDataUpdate)
    {
        materialProperty = new MaterialPropertyBlock();
        materialProperty.SetColor("_BaseColor", data.color);

        newDataUpdate.FixData();
        yield return InitializeFixedBuffer(newDataUpdate.positions, newDataUpdate.rotations, newDataUpdate.scales);
    }

    public override void DrawMeshes()
    {
        if (fullComputeBuffer == null)
        {
            return;
        }

        // set wind properties
        if (this.data.windZone != null)
        {
            data.material.SetVector(windDirectionId, this.data.windZone.transform.forward);
            data.material.SetFloat(windMainId, this.data.windZone.windMain * this.data.detailWindStrength);
            data.material.SetFloat(windTurbulenceId, this.data.windZone.windTurbulence);
            data.material.SetFloat(windPulseFrequencyId, this.data.windZone.windPulseFrequency);
            data.material.SetFloat(windPulseMagnitudeId, this.data.windZone.windPulseMagnitude);
        }
        else
        {
            data.material.SetFloat(windMainId, 0);
        }

        // read volatile data info
        ComputeBuffer.CopyCount(volatileComputeBuffer, bufferWithVolatileArgs, sizeof(uint));

        // draw all meshes
        Graphics.DrawMeshInstancedIndirect(data.mesh, 0, data.material, bounds, bufferWithVolatileArgs, argsOffset: 0, materialProperty, data.castShadows);
    }

    /// <summary>
    /// Initialize the fixed buffer. This should not be called very often.
    /// </summary>
    /// <param name="positions"></param>
    /// <param name="rotations"></param>
    /// <param name="scales"></param>
    private IEnumerator InitializeFixedBuffer(Vector3[] positions, Quaternion[] rotations, Vector3[] scales)
    {
        bounds = new Bounds(Vector3.zero, Vector3.one);
        var bufferData = new ComputeBufferData[positions.Length];
        UnityEngine.Random.InitState(1);
        for (int i = 0; i < positions.Length; i++)
        {
            bufferData[i] = new ComputeBufferData();
            bufferData[i]._Matrix = new Matrix4x4();
            bufferData[i]._Matrix.SetTRS(positions[i], rotations[i], scales[i]);
            bufferData[i]._MatrixInverse = bufferData[i]._Matrix.inverse;
            bufferData[i]._Info = new Vector4(UnityEngine.Random.value, 0, 0, 0); // can be used to pass arbitrary, instance based data to the shader.

            bounds.Expand(positions[i]);

            if(i % 100000 == 0)
            {
                // this does not work properly...
                // yield return new WaitForEndOfFrame();
            }
        }

        // indirect args
        uint numIndices = data.mesh != null ? data.mesh.GetIndexCount(0) : 0;
        volatileArgs[0] = numIndices;
        volatileArgs[1] = (uint)bufferData.Length;

        if (fullComputeBuffer != null)
            fullComputeBuffer.Release();
        if (bufferWithVolatileArgs != null)
            bufferWithVolatileArgs.Release();

        if (positions.Length == 0)
        {
            fullComputeBuffer = null;
        }
        else
        {
            bufferWithVolatileArgs = new ComputeBuffer(1, volatileArgs.Length * sizeof(uint), ComputeBufferType.IndirectArguments);
            bufferWithVolatileArgs.SetData(volatileArgs);

            fullComputeBuffer = new ComputeBuffer(positions.Length, CB_SIZE);
            fullComputeBuffer.SetData(bufferData);

            computeShader.SetBuffer(0, fullBufferId, fullComputeBuffer);

            // Note that even the volatile append buffer must have a large size.
            // Appending if the buffer is full will crash the GPU!
            if (lastVolatileComputeBufferCount < positions.Length)
            {
                if (volatileComputeBuffer != null)
                    volatileComputeBuffer.Release();
                volatileComputeBuffer = new ComputeBuffer(positions.Length, CB_SIZE, ComputeBufferType.Append);
                volatileComputeBuffer.SetCounterValue(0); // necessary to set this here, otherwise the GPU can crash in Play Mode...

                lastVolatileComputeBufferCount = positions.Length;
                computeShader.SetBuffer(0, volatileBufferId, volatileComputeBuffer);
            }

            data.material.SetBuffer(volatileBufferId, volatileComputeBuffer);
        }

        yield return null;
    }

    /// <summary>
    /// Move data from the entire buffer to the volatile buffer where it is displayed by instancing.
    /// </summary>
    /// <param name="prototypeIndex"></param>
    /// <param name="terrain"></param>
    /// <param name="parent"></param>
    /// <returns></returns>
    public override IEnumerator RecomputeDetailData(int prototypeIndex, Terrain terrain, TerrainDetailInstancing parent)
    {
        if(!Application.isPlaying || !fullBufferInitialized)
        {
            yield return CreateFullBuffer(prototypeIndex, terrain, parent);
        }

        if (fullBufferInitialized)
        {
            // the computation is done on a square grid of threads, so set the resolution accordingly
            resolution = Mathf.CeilToInt(Mathf.Sqrt(fullComputeBuffer.count));

            // set fixed data and buffers
            computeShader.SetInt(resolutionId, resolution);
            computeShader.SetInt(totalObjectsId, fullComputeBuffer.count);

            // set general terrain detail data
            computeShader.SetVector(cameraPositionId, Camera.main.transform.position);
            computeShader.SetVector(cameraDirectionId, Camera.main.transform.forward);
            computeShader.SetFloat(fadeDistanceId, parent.FadeDetailsAtDistance ? parent.FadeRange : 0);
            computeShader.SetFloat(cullDistanceId, terrain.detailObjectDistance);
            computeShader.SetFloat(cullAngleId, Mathf.Max(0, Mathf.Cos(Camera.main.fieldOfView / 180f * Mathf.PI)));

            // set the volatile buffer info
            volatileComputeBuffer.SetCounterValue(0); // reset the counter so that we start filling at zero

            // move data from fixed buffer to volatile buffer
            int groups = Mathf.CeilToInt(resolution / 8f);
            computeShader.Dispatch(0, groups, groups, 1);
        }

        yield return null;
    }

    private IEnumerator CreateFullBuffer(int prototypeIndex, Terrain terrain, TerrainDetailInstancing parent)
    {
        var detailPositionCreator = new TerrainDetailKernelInstancingDirect(this);
        // compute all terrain details
        yield return detailPositionCreator.RecomputeDetailData(prototypeIndex, terrain, parent, cullByDistance: false);
    }

    public override void Release()
    {
        OnDisable();
    }

    public void OnDispose()
    {
        OnDisable();
    }

    public void OnDisable()
    {
        if (fullComputeBuffer != null)
        {
            fullComputeBuffer.Release();
        }
        fullComputeBuffer = null;

        if (volatileComputeBuffer != null)
        {
            volatileComputeBuffer.Release();
        }
        volatileComputeBuffer = null;

        if (bufferWithVolatileArgs != null)
        {
            bufferWithVolatileArgs.Release();
        }
        bufferWithVolatileArgs = null;
    }

    public override int Count()
    {
        return lastVolatileComputeBufferCount > 0 ? lastVolatileComputeBufferCount : 0;
    }
}
