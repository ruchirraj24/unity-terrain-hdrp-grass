#pragma kernel CSProceduralInstancing

#define PI 3.14159265358979323846

struct CB_Data
{
    float4x4 _Matrix;
    float4x4 _MatrixInverse;
    float4 _Info;
};

RWStructuredBuffer<float> _VolatileBufferArgs;

// we move data from Full -> Volatile
RWStructuredBuffer<CB_Data> _FullInstancingData;
AppendStructuredBuffer<CB_Data> _VolatileInstancingData;

float3 _CameraPosition;
float3 _CameraDirection;
float _CullDistance;
float _CullAngle;
float _FadeDistance;
uint _Resolution;
uint _TotalObjects;

float vec_dist(float3 v1, float3 v2)
{
    float xd, yd, zd;
    xd = v1.x - v2.x;
    yd = v1.y - v2.y;
    zd = v1.z - v2.z;
    return sqrt(xd * xd + yd * yd + zd * zd);
}

float3 vec_from_mat(float4x4 _mat)
{
    float3 pos;
    pos[0] = _mat[0][3];
    pos[1] = _mat[1][3];
    pos[2] = _mat[2][3];
    return pos;
}

float vec_dot_normalized(float3 v1, float3 v2)
{
    v1 = v1 / sqrt(v1.x * v1.x + v1.y * v1.y + v1.z * v1.z);
    v2 = v2 / sqrt(v2.x * v2.x + v2.y * v2.y + v2.z * v2.z);
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

float4x4 move_mat_from_obj(float4x4 translate_mat, float multiplier)
{
    float4x4 _mat;
    _mat = 0;

    _mat[0][3] = translate_mat[0][3] * multiplier;
    _mat[1][3] = translate_mat[1][3] * multiplier;
    _mat[2][3] = translate_mat[2][3] * multiplier;
    _mat[3][3] = 1;
    return _mat;
}

CB_Data scale_object(CB_Data _obj, float scale)
{
    float4x4 scaleMat;
    scaleMat = 0;

    scaleMat[0][0] = scale;
    scaleMat[1][1] = scale;
    scaleMat[2][2] = scale;
    scaleMat[3][3] = 1;

    _obj._Matrix = mul(_obj._Matrix, scaleMat);

    scaleMat = 0;

    scaleMat[0][0] = 1.0 / scale;
    scaleMat[1][1] = 1.0 / scale;
    scaleMat[2][2] = 1.0 / scale;
    scaleMat[3][3] = 1;

    _obj._MatrixInverse = mul(scaleMat, _obj._MatrixInverse);
    return _obj;
}

[numthreads(8,8,1)]
void CSProceduralInstancing(uint3 id : SV_DispatchThreadID)
{
    uint localId;
    localId = id.x + id.y * _Resolution;
    if (id.x < _Resolution && id.y < _Resolution && localId < _TotalObjects) {
        CB_Data _instancingObject;
        _instancingObject = _FullInstancingData[localId];

        float3 _obj_pos;
        _obj_pos = vec_from_mat(_instancingObject._Matrix);
        if (vec_dot_normalized(_obj_pos - _CameraPosition, _CameraDirection) > _CullAngle)
        {
            float _obj_distance;
            _obj_distance = vec_dist(_obj_pos, _CameraPosition);
            if (_obj_distance < _CullDistance)
            {
                if (_FadeDistance > 0)
                {
                    float _scaleModifier;
                    _scaleModifier = clamp((_CullDistance - _obj_distance) / _FadeDistance, 0.0, 1.0);

                    _instancingObject = scale_object(_instancingObject, _scaleModifier);
                }
                _VolatileInstancingData.Append(_instancingObject);
            }
        }
    }
}
