using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UIElements;
using static TerrainDetailInstancing;

[CustomEditor(typeof(TerrainDetailInstancing))]
[CanEditMultipleObjects]
public class TerrainEditing : Editor
{
    private SerializedProperty terrainSerialized;
    private SerializedProperty inEditorSerialized;
    private SerializedProperty useInstancingSerialized;
    private SerializedProperty castShadowsSerialized;
    private int detailMeshesCount;
    private bool useInEditor;
    private TerrainDetailInstancing.InstancingType useInstancing;
    private ShadowCastingMode castShadows;

    public void OnEnable()
    {
        terrainSerialized = serializedObject.FindProperty("terrain");
        inEditorSerialized = serializedObject.FindProperty("UseInEditor");
        useInstancingSerialized = serializedObject.FindProperty("UseProceduralInstancing");
        castShadowsSerialized = serializedObject.FindProperty("castShadows");
        detailMeshesCount = -1;
        useInEditor = inEditorSerialized.boolValue;
        castShadows = (ShadowCastingMode)castShadowsSerialized.enumValueIndex;
    }

    public override void OnInspectorGUI()
    {
        base.DrawDefaultInspector();

        serializedObject.Update();

        var terrain = (Terrain)this.terrainSerialized.objectReferenceValue;
        var recreateInstancers = false;

        if(terrain == null)
        {
            return;
        }

        if (detailMeshesCount != terrain.terrainData.detailPrototypes.Length)
        {
            recreateInstancers = true;
        }
        if (inEditorSerialized.boolValue != this.useInEditor && inEditorSerialized.boolValue == true)
        {
            recreateInstancers = true;
        }
        if ((InstancingType)useInstancingSerialized.enumValueIndex != this.useInstancing)
        {
            recreateInstancers = true;
        }
        if ((ShadowCastingMode)castShadowsSerialized.enumValueIndex != this.castShadows)
        {
            recreateInstancers = true;
        }

        if (recreateInstancers && !Application.isPlaying)
        {
            ((TerrainDetailInstancing)target).RecreateInstancers();
        }

        detailMeshesCount = terrain.terrainData.detailPrototypes.Length;
        useInEditor = inEditorSerialized.boolValue;
        useInstancing = (InstancingType)useInstancingSerialized.enumValueIndex;
        castShadows = (ShadowCastingMode)castShadowsSerialized.enumValueIndex;

        serializedObject.ApplyModifiedProperties();

        TerrainDetailInstancing terrainInstancer = (TerrainDetailInstancing)target;

        if (terrainInstancer != null)
        {
            GUILayout.Label("Currently drawing " + terrainInstancer.DetailCount() + " details.");
        }
    }
}
